import requests
from pywebio.input import *
from pywebio.output import *
import firebase
from backend import L3_Backend

l3_backend = L3_Backend()

def dashboard():
    global kiosk_id
    credentials = input_group("Credentials",[
        input("Email:", name="email", placeholder='Email', required=True),
        input("Password:", name="password", type=PASSWORD, placeholder='Password', required=True)
    ])
    print(credentials)

    #TODO uncoment before deployment
    # if credentials['email'] == 'developer@6dbytes.com' and credentials['password'] == 'Raspberri':
    #     print('access granted')
    #
    # else:
    #     print('access not granted')
    #     put_text("The identification failed")
    #     return

    locations = l3_backend.get_locations()
    locations['Kiosk #4'] = 6100728784355328
    location = select('Which location would you like to select?', locations)

    kiosk_id = locations[location]
    kiosk_info = firebase.kiosk_info
    kiosk_status = firebase.get_kiosk_status(kiosk_id)

    jar_table = firebase.get_jar_position(kiosk_id)

    put_text(str(location) + " is currently in " + str(kiosk_status))
    put_table(jar_table)

if __name__ == '__main__':
    dashboard()
