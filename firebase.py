import requests
import firebase_admin
from firebase_admin import db
from firebase_admin import credentials

cred = credentials.Certificate("kiosk-info.json")
kiosk_info = firebase_admin.initialize_app(cred, {'databaseURL': 'https://pacific-apex-122622.firebaseio.com/'})

def get_kiosk_status(kiosk_id):
    kiosk_path = 'company/5649391675244544/kiosk/' + str(kiosk_id) + '/'
    kiosk_status_path = kiosk_path + 'kiosk_state' + '/'
    ref = db.reference(str(kiosk_status_path), kiosk_info)
    kiosk_status = ref.get()["value"]
    return kiosk_status

def get_current_orders(kiosk_id):
    kiosk_path = 'company/5649391675244544/kiosk/' + str(kiosk_id) + '/'
    kiosk_order_path = kiosk_path + 'orders' + '/'

    ref = db.reference(str(kiosk_order_path), kiosk_info)
    orders = ref.get()
    return orders

def get_jar_position(kiosk_id):
    # add expected delivery time
    kiosk_path = 'company/5649391675244544/kiosk/' + str(kiosk_id) + '/'
    kiosk_order_path = kiosk_path + 'orders' + '/'

    jar_table = [['Order #', 'Jar', 'Jar Place']]

    ref = db.reference(str(kiosk_order_path), kiosk_info)
    orders = ref.get()
    for item in orders:
        order = orders[item]['value']
        if 'jar_name' and 'jar_place' in order:
            jar_table.append([order['id'], order['jar_name'], order['jar_place']])
        elif 'jar_name' in order:
            jar_table.append([order['id'], order['jar_name'], 'N/A'])
        else:
            jar_table.append([order['id'], 'N/A', 'N/A'])
    return jar_table

    