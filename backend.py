import firebase
import requests
from threading import Thread
import time
# make backend into a class
# create variable within class for all orders at all locations

class L3_Backend:
    def __init__(self):

        self.running = False
        self.headers = {
            'Authorization': 'Uab5qxVNPjZVhvAw1ctqVbvbM0BqDEnG7aGw3Po9X3zFaFO5wvssp8pmAzgFaZvRbtMzT2OyLA4NoRusCFMpAmEXzfYpueCxD84cQEvOF+ew8GUDRq40oZHG/79zdMfl',
            'Accept': 'application/json',
        }

        self.kiosk_to_monitor = ['6100728784355328'] #list of the kiosk where the service can take action

        self.start()

    def start(self):
        self.running = True
        thread = Thread(target=self.__monitor_orders, args=())
        thread.start()

    def __monitor_orders(self):
        # while loops to scan current orders in kiosk
        # check orders that have expected time over 10 min - check with current time
        # find device thats causing kiosk to go provisioning
        # print(dashboard.kiosk_id)
        orders = firebase.get_current_orders(6100728784355328)
        while(self.running):
            try:
                print('running')
                # loop through each kiosk
                # if kiosk provisioning show device
                # get start time and expected delivery time for each order in each kiosk
                if firebase.get_kiosk_status(6100728784355328) == "PROVISIONING":

                    # scan for device error
                    print("error")
                for item in orders:
                    order = orders[item]['value']
                    if (order['expected_delivery_time'] > 10):
                        print(order)

                time.sleep(3)

            except Exception as e:
                print("Server error: " + str(e))

    def stop(self):
        self.running = False

    def get_locations(self):
        locations = {}
        response = requests.get('https://api.6dbytes.com/v1/kiosk/', headers=self.headers)

        for item in response.json()['value']:
            locations[item['name']] = item['kioskId']
        return locations

    def send_diagnostic_step(self, kiosk_id, device_id, process, parameters = []):
        '''
        :param kiosk_id: in string format
        :param device_id: in string format
        :param process: process name in string format example ScaleCheck
        :param parameters: parameters of the process example:
        [
        {"name": "device parameter", "value_type": "DEVICE", "value": {"id": '4574342418530304', "name": "Solid Dispenser1", "abstract": False}}
        ]

        "parameters": [
        {"name": "number parameter", "value_type": "NUMBER", "value": "number as string"},
        {"name": "text parameter", "value_type": "TEXT", "value": "text"},
        {"name": "device parameter", "value_type": "DEVICE", "value": {"id": <device_id>, "name": "device name", "abstract": False}}]

        :return:
        '''

        kiosk_id = '6100728784355328' #force it to be K4 for now to avoid production interference
        data = {"device": {"abstract": False, "id": device_id}, "process": process, "parameters": []}
        url = 'https://api.6dbytes.com/v1/admin/kiosk/' + kiosk_id + '/send_diagnostics_step'
        response = self.__send_post_request(url, data)

    def __send_post_request(self, url, data):
        response = requests.post(url, headers=self.headers, json=data)
        if response.json()['success'] == True:
            print('The response is: ' + str(response.json()))
        else:
            print('Request not successful')
        return response.json()

    def test_diagnostic_step(self):
        response = self.send_diagnostic_step('6100728784355328', '4574342418530304', 'ScaleCheck')


#L3_Backend().test_diagnostic_step()
